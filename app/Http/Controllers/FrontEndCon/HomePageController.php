<?php

namespace App\Http\Controllers\FrontEndCon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomePageController extends Controller
{
    private $dir;

    public function __construct()
    {
        $this->dir = '1t0ioH4oqzEvGqHsWd2LgYQ5V5b6V5grT';
    }

    public function index()
    {
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($this->dir, $recursive));

        $files = $contents->where('type', '=', 'file');
        return view('public.home', [
            'files' => $files
        ]);
    }

    public function download(Request $request)
    {
        $filename = $request->file;
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($this->dir, $recursive));

        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!

        //return $file; // array with file info

        $rawData = Storage::cloud()->get($file['path']);
        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename=$filename");
    }
}
