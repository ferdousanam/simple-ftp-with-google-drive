@extends('public.layouts.app')

@section('content')
    <div class="container">
        <div class="card">
          <div class="card-header">
            Download Useful Software
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-striped table-sm">
                      <thead>
                      <tr>
                          <th>Software Name</th>
                          <th>Download Link</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($files as $file)
                          <tr>
                              <td>{{ $file['name'] }}</td>
                              <td><a href="{{ url('download') }}?file={{ $file['name'] }}">Download</a></td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
        </div>
    </div>
@endsection
